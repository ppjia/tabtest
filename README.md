## Instruction

Please clone the project, run `pod update` and open `Quiz.xcworkspace` .



## Architecture

This project uses MVVM. The `QuestionViewModel` mantains question and answer status, all the question related views/ViewController have items bound with the viewModel's properties. It uses RxSwift to implement such data binding.

Used `SnapKit` (previousely `Masonry` for ObjC) as autolayout engine to write clean constraints. 



## Testing

Unit test covers `DataProvider` and `QuestionViewModel`, to test the json unwrapping and viewModel's logic.

UITest covers the whole process of answering and score counting.



## Environment

Xcode 10.2.1, Swift 5, Cocoapods 1.6.1