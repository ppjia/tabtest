//
//  QuizTests.swift
//  QuizTests
//
//  Created by Rui Jia on 23/5/19.
//  Copyright © 2019 Rui. All rights reserved.
//

import XCTest
@testable import Quiz

class QuizTests: XCTestCase {

    func testDataProvider() {
        let questions = DataProvider.loadQuestions()
        XCTAssertTrue(questions.count == 5)
    }

    func testQuestionViewModel() {
        let questions = DataProvider.loadQuestions()
        var viewModel = QuestionViewModel(questions: questions)
        viewModel.commitAnswer(false)
        XCTAssertTrue(viewModel.results.value.last == 0)
        viewModel.moveToNext()
        XCTAssertTrue(viewModel.currentIndex.value == 1)
        viewModel.commitAnswer(true)
        XCTAssertTrue(viewModel.results.value.last == 1)
        viewModel.moveToNext()
        viewModel.commitAnswer(false)
        viewModel.moveToNext()
        viewModel.commitAnswer(false)
        viewModel.moveToNext()
        viewModel.commitAnswer(true)
        XCTAssertTrue(viewModel.score == 4)
        XCTAssertTrue(viewModel.isCurrentTheLastOne)
    }
}
