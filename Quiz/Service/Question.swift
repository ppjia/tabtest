//
//  Question.swift
//  Quiz
//
//  Created by Rui Jia on 23/5/19.
//  Copyright © 2019 Rui. All rights reserved.
//

import Foundation

struct Quiz: Codable {
    let domain: String
    let questions: [Question]
}

struct Question: Codable {
    let question: String
    let answer: Bool
}
