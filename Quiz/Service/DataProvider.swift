//
//  DataProvider.swift
//  Quiz
//
//  Created by Rui Jia on 23/5/19.
//  Copyright © 2019 Rui. All rights reserved.
//

import Foundation

struct DataProvider {
    static func loadQuestions() -> [Question] {
        let quizData = try! Data(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: "quiz", ofType: "json")!))
        guard let quiz = try? JSONDecoder().decode(Quiz.self, from: quizData) else { return [] }
        return quiz.questions
    }
}
