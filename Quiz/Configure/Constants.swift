//
//  Constants.swift
//  Quiz
//
//  Created by Rui Jia on 24/5/19.
//  Copyright © 2019 Rui. All rights reserved.
//

import Foundation

struct Constants {
    static let identifier_button_true = "identifier_button_true"
    static let identifier_button_false = "identifier_button_false"
    static let identifier_button_next = "identifier_button_next"
    static let identifier_text_score = "identifier_text_score"
}
