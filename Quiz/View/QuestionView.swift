//
//  QuestionView.swift
//  Quiz
//
//  Created by Rui Jia on 23/5/19.
//  Copyright © 2019 Rui. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class QuestionView: UIView {
    private let disposeBag = DisposeBag()
    
    private let questionLabel = UILabel()
    private let trueButton = UIButton()
    private let falseButton = UIButton()
    
    private var viewModel: QuestionViewModel
    
    init(viewModel: QuestionViewModel) {
        self.viewModel = viewModel
        
        super.init(frame: .zero)
        composeView()
        binding()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func composeView() {
        questionLabel.font = UIFont.systemFont(ofSize: 28)
        questionLabel.numberOfLines = 0
        
        trueButton.setTitleColor(.black, for: .normal)
        trueButton.setTitle("True", for: .normal)
        trueButton.titleLabel?.font = UIFont.systemFont(ofSize: 28)
        trueButton.setTitleColor(.blue, for: .normal)
        trueButton.setTitleColor(.gray, for: .disabled)
        trueButton.accessibilityIdentifier = Constants.identifier_button_true
        
        falseButton.setTitleColor(.black, for: .normal)
        falseButton.setTitle("False", for: .normal)
        falseButton.titleLabel?.font = UIFont.systemFont(ofSize: 28)
        falseButton.setTitleColor(.blue, for: .normal)
        falseButton.setTitleColor(.gray, for: .disabled)
        falseButton.accessibilityIdentifier = Constants.identifier_button_false
        
        let buttonStackView = UIStackView()
        buttonStackView.axis = .horizontal
        buttonStackView.alignment = .center
        buttonStackView.distribution = .fillEqually
        buttonStackView.spacing = 60
        buttonStackView.addArrangedSubview(trueButton)
        buttonStackView.addArrangedSubview(falseButton)
        
        
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.alignment = .leading
        stackView.distribution = .fillProportionally
        stackView.spacing = 40
        stackView.addArrangedSubview(questionLabel)
        stackView.addArrangedSubview(buttonStackView)
        addSubview(stackView)
        
        buttonStackView.snp.makeConstraints { make in
            make.height.equalTo(44)
        }
        stackView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
}

private extension QuestionView {
    
    func binding() {
        trueButton.rx.tap.bind { self.buttonTapOn(answer: true) }.disposed(by: disposeBag)
        falseButton.rx.tap.bind { self.buttonTapOn(answer: false) }.disposed(by: disposeBag)
        
        // When question changed, update buttons stutus and question content.
        viewModel.currentQuestion.asObservable().subscribe(onNext: { question in
            self.trueButton.isEnabled = true
            self.falseButton.isEnabled = true
            self.questionLabel.text = question?.question
        }).disposed(by: disposeBag)
    }
    
    func buttonTapOn(answer: Bool) {
        viewModel.commitAnswer(answer)
        
        trueButton.isEnabled = false
        falseButton.isEnabled = false
    }
}
