//
//  ResultView.swift
//  Quiz
//
//  Created by Rui Jia on 23/5/19.
//  Copyright © 2019 Rui. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ResultView: UIView {
    
    enum Result: String {
        case correct = "Your answer is correct!"
        case incorrect = "Your answer is wrong!"
    }
    
    private let disposeBag = DisposeBag()
    
    private var viewModel: QuestionViewModel
    private let resultLabel = UILabel()
    private let nextButton = UIButton()
    
    init(viewModel: QuestionViewModel) {
        self.viewModel = viewModel
        
        super.init(frame: .zero)
        composeView()
        binding()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func composeView() {
        resultLabel.font = UIFont.systemFont(ofSize: 28)
        resultLabel.textAlignment = .center
        addSubview(resultLabel)
        
        nextButton.setTitle("Next Question", for: .normal)
        nextButton.setTitleColor(.blue, for: .normal)
        nextButton.titleLabel?.font = UIFont.systemFont(ofSize: 28)
        nextButton.accessibilityIdentifier = Constants.identifier_button_next
        addSubview(nextButton)
        
        resultLabel.snp.makeConstraints { make in
            make.left.top.right.equalToSuperview()
            make.height.equalTo(50)
        }
        
        nextButton.snp.makeConstraints { make in
            make.top.equalTo(resultLabel.snp.bottom)
            make.bottom.centerX.equalToSuperview()
            make.height.equalTo(50)
            make.width.equalTo(180)
        }
    }
}

private extension ResultView {
    
    func binding() {
        nextButton.rx.tap.bind { self.viewModel.moveToNext() }.disposed(by: disposeBag)
        
        // When new question comes up, hide result view.
        viewModel.currentQuestion.asObservable().subscribe(onNext: { _ in
            self.isHidden = true
        }).disposed(by: disposeBag)
        
        // When results has been updated, change status of result view elements.
        viewModel.results.asObservable().subscribe(onNext: { updatedResults in
            guard updatedResults.count > 0 else { return }
            self.isHidden = false
            let result = updatedResults.last == 1
            self.resultLabel.text = result ? Result.correct.rawValue : Result.incorrect.rawValue
            self.nextButton.isHidden = self.viewModel.isCurrentTheLastOne
        }).disposed(by: disposeBag)
    }
}
