//
//  QuestionViewModel.swift
//  Quiz
//
//  Created by Rui Jia on 23/5/19.
//  Copyright © 2019 Rui. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

struct QuestionViewModel {
    private let questions: [Question]
    
    var currentIndex: BehaviorRelay<Int> = BehaviorRelay(value: 0)
    var results: BehaviorRelay<[Int]> = BehaviorRelay(value: [])
    var currentQuestion: BehaviorRelay<Question?>
    
    init(questions: [Question]) {
        self.questions = questions
        currentQuestion = BehaviorRelay(value: questions.first)
    }
    
    // Summarize the results to get total score.
    var score: Int {
        return results.value.reduce(0, { $0 + $1 })
    }
    
    var isCurrentTheLastOne: Bool {
        return currentIndex.value == questions.count - 1
    }
    
    func question(at index: Int) -> Question? {
        guard index < questions.count else { return nil }
        return questions[index]
    }
}

// Actions.
extension QuestionViewModel {
    mutating func commitAnswer(_ answer: Bool) {
        guard let question = currentQuestion.value else { return }
        
        // If correct, append 1 in results list, otherwise put 0.
        let result = answer == question.answer ? 1 : 0
        results.accept(results.value + [result])
    }
    
    mutating func moveToNext() {
        currentIndex.accept(currentIndex.value + 1)
        currentQuestion.accept(question(at: currentIndex.value))
    }
}
