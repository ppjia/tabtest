//
//  LandingViewController.swift
//  Quiz
//
//  Created by Rui Jia on 23/5/19.
//  Copyright © 2019 Rui. All rights reserved.
//

import UIKit
import SnapKit

class LandingViewController: UIViewController {
    private let scoreLabel = UILabel()
    private let playButton = UIButton()
    
    override func loadView() {
        super.loadView()
        
        let view = UIView()
        view.backgroundColor = .white
        
        let scoreView = composeScoreView()
        view.addSubview(scoreView)
        
        playButton.setTitleColor(.blue, for: .normal)
        playButton.titleLabel?.font = UIFont.systemFont(ofSize: 28)
        playButton.setTitle("Play", for: .normal)
        playButton.addTarget(self, action: #selector(didTapPlay), for: .touchUpInside)
        view.addSubview(playButton)
        
        scoreView.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.left.right.equalToSuperview()
        }
        
        playButton.snp.makeConstraints { make in
            make.height.equalTo(44)
            make.width.equalTo(100)
            make.centerX.equalToSuperview()
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom).inset(40)
        }
        
        self.view = view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scoreLabel.text = "-"
    }
}

private extension LandingViewController {
    func composeScoreView() -> UIView {
        let titleLabel = UILabel()
        titleLabel.text = "Score"
        titleLabel.font = UIFont.systemFont(ofSize: 28)
        titleLabel.textAlignment = .center
        view.addSubview(titleLabel)
        
        scoreLabel.font = UIFont.systemFont(ofSize: 28)
        scoreLabel.textAlignment = .center
        scoreLabel.accessibilityIdentifier = Constants.identifier_text_score
        view.addSubview(scoreLabel)
        
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.alignment = .center
        stackView.spacing = 40
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(scoreLabel)
        
        return stackView
    }
}

// Action
private extension LandingViewController {
    @objc func didTapPlay() {
        let viewModel = QuestionViewModel(questions: DataProvider.loadQuestions())
        let questionViewController = QuestionViewController(viewModel: viewModel) { [weak self] score in
            self?.scoreLabel.text = "\(score)"
        }
        present(UINavigationController(rootViewController: questionViewController), animated: true, completion: nil)
    }
}
