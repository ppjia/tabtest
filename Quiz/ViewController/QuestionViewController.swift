//
//  QuestionViewController.swift
//  Quiz
//
//  Created by Rui Jia on 23/5/19.
//  Copyright © 2019 Rui. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

// Callback for passing score.
typealias FinishedAction = ((Int) -> Void)

class QuestionViewController: UIViewController {
    private let disposeBag = DisposeBag()
    private let viewModel: QuestionViewModel
    
    private let questionView: QuestionView
    private let resultView: ResultView
    
    private let finishedAction: FinishedAction
    
    init(viewModel: QuestionViewModel, finishedAction: @escaping FinishedAction) {
        self.viewModel = viewModel
        self.finishedAction = finishedAction
        
        questionView = QuestionView(viewModel: viewModel)
        resultView = ResultView(viewModel: viewModel)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        
        let view = UIView()
        view.backgroundColor = .white
        
        view.addSubview(questionView)
        view.addSubview(resultView)
        
        questionView.snp.makeConstraints { make in
            make.left.right.equalToSuperview().inset(20)
            make.top.equalTo(view.safeAreaLayoutGuide.snp.top).offset(40)
        }
        resultView.snp.makeConstraints { make in
            make.left.right.equalToSuperview().inset(20)
            make.bottom.equalTo(view.safeAreaLayoutGuide.snp.bottom).inset(20)
        }
        
        self.view = view
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(didTapDone))
        
        // Binding title with current index.
        viewModel.currentIndex.asObservable().map { "Question \($0 + 1)" }.bind(to: self.rx.title).disposed(by: disposeBag)
    }
    
    @objc private func didTapDone() {
        dismiss(animated: true) { [weak self] in
            guard let self = self else { return }
            self.finishedAction(self.viewModel.score)
        }
    }
}
