//
//  QuizUITests.swift
//  QuizUITests
//
//  Created by Rui Jia on 23/5/19.
//  Copyright © 2019 Rui. All rights reserved.
//

import XCTest

class QuizUITests: XCTestCase {
    private let app = XCUIApplication()

    override func setUp() {
        continueAfterFailure = false
        app.launch()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testQuestion() {
        app.buttons.firstMatch.tap()
        let trueButton = app.buttons[Constants.identifier_button_true]
        let falseButton = app.buttons[Constants.identifier_button_false]
        let nextButton = app.buttons[Constants.identifier_button_next]
        XCTAssertTrue(trueButton.exists)
        XCTAssertTrue(falseButton.exists)
        XCTAssertFalse(nextButton.exists)
        
        trueButton.tap()
        
        XCTAssertFalse(trueButton.isEnabled)
        XCTAssertFalse(falseButton.isEnabled)
        XCTAssertTrue(app.staticTexts["Your answer is correct!"].exists)
        XCTAssertTrue(nextButton.exists)
        
        nextButton.tap()
        
        XCTAssertTrue(app.staticTexts["A struct created as a var can have its variables changed?"].exists)
        
        trueButton.tap()
        sleep(1)
        nextButton.tap()
        trueButton.tap()
        sleep(1)
        nextButton.tap()
        falseButton.tap()
        sleep(1)
        nextButton.tap()
        trueButton.tap()
        sleep(1)
        
        app.navigationBars.buttons.firstMatch.tap()
        XCTAssertEqual(app.staticTexts.element(matching:.any, identifier: Constants.identifier_text_score).label, "4")
        
    }

}
